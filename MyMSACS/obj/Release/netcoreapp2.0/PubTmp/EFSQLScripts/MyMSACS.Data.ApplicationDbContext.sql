﻿IF OBJECT_ID(N'__EFMigrationsHistory') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE TABLE [AspNetRoles] (
        [Id] nvarchar(450) NOT NULL,
        [ConcurrencyStamp] nvarchar(max) NULL,
        [Name] nvarchar(256) NULL,
        [NormalizedName] nvarchar(256) NULL,
        CONSTRAINT [PK_AspNetRoles] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE TABLE [AspNetUsers] (
        [Id] nvarchar(450) NOT NULL,
        [AccessFailedCount] int NOT NULL,
        [ConcurrencyStamp] nvarchar(max) NULL,
        [Email] nvarchar(256) NULL,
        [EmailConfirmed] bit NOT NULL,
        [LockoutEnabled] bit NOT NULL,
        [LockoutEnd] datetimeoffset NULL,
        [NormalizedEmail] nvarchar(256) NULL,
        [NormalizedUserName] nvarchar(256) NULL,
        [PasswordHash] nvarchar(max) NULL,
        [PhoneNumber] nvarchar(max) NULL,
        [PhoneNumberConfirmed] bit NOT NULL,
        [SecurityStamp] nvarchar(max) NULL,
        [TwoFactorEnabled] bit NOT NULL,
        [UserName] nvarchar(256) NULL,
        CONSTRAINT [PK_AspNetUsers] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE TABLE [Degree] (
        [DegreeId] int NOT NULL,
        [DegreeAbbrev] nvarchar(10) NOT NULL,
        [DegreeName] nvarchar(50) NOT NULL,
        CONSTRAINT [PK_Degree] PRIMARY KEY ([DegreeId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE TABLE [DegreeStatus] (
        [DegreeStatusId] int NOT NULL,
        [Status] nvarchar(15) NOT NULL,
        CONSTRAINT [PK_DegreeStatus] PRIMARY KEY ([DegreeStatusId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE TABLE [RequirementStatus] (
        [RequirementStatusId] int NOT NULL,
        [Status] nvarchar(15) NOT NULL,
        CONSTRAINT [PK_RequirementStatus] PRIMARY KEY ([RequirementStatusId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE TABLE [Student] (
        [StudentId] int NOT NULL,
        [FamilyName] nvarchar(50) NULL,
        [GivenName] nvarchar(50) NOT NULL,
        CONSTRAINT [PK_Student] PRIMARY KEY ([StudentId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE TABLE [AspNetRoleClaims] (
        [Id] int NOT NULL IDENTITY,
        [ClaimType] nvarchar(max) NULL,
        [ClaimValue] nvarchar(max) NULL,
        [RoleId] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE TABLE [AspNetUserClaims] (
        [Id] int NOT NULL IDENTITY,
        [ClaimType] nvarchar(max) NULL,
        [ClaimValue] nvarchar(max) NULL,
        [UserId] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE TABLE [AspNetUserLogins] (
        [LoginProvider] nvarchar(450) NOT NULL,
        [ProviderKey] nvarchar(450) NOT NULL,
        [ProviderDisplayName] nvarchar(max) NULL,
        [UserId] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY ([LoginProvider], [ProviderKey]),
        CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE TABLE [AspNetUserRoles] (
        [UserId] nvarchar(450) NOT NULL,
        [RoleId] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY ([UserId], [RoleId]),
        CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE TABLE [AspNetUserTokens] (
        [UserId] nvarchar(450) NOT NULL,
        [LoginProvider] nvarchar(450) NOT NULL,
        [Name] nvarchar(450) NOT NULL,
        [Value] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY ([UserId], [LoginProvider], [Name]),
        CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE TABLE [DegreeRequirement] (
        [DegreeRequirementId] int NOT NULL,
        [DegreeId] int NOT NULL,
        [RequirementAbbrev] nvarchar(10) NOT NULL,
        [RequirementName] nvarchar(60) NOT NULL,
        [RequirementNumber] int NOT NULL,
        CONSTRAINT [PK_DegreeRequirement] PRIMARY KEY ([DegreeRequirementId]),
        CONSTRAINT [FK_DegreeRequirement_Degree_DegreeId] FOREIGN KEY ([DegreeId]) REFERENCES [Degree] ([DegreeId]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE TABLE [StudentDegreePlan] (
        [StudentDegreePlanId] int NOT NULL,
        [DegreeId] int NOT NULL,
        [DegreeStatusId] int NOT NULL,
        [IncludesInternship] bit NOT NULL,
        [PlanAbbrev] nvarchar(20) NOT NULL,
        [PlanName] nvarchar(50) NOT NULL,
        [PlanNumber] int NOT NULL,
        [StudentId] int NOT NULL,
        CONSTRAINT [PK_StudentDegreePlan] PRIMARY KEY ([StudentDegreePlanId]),
        CONSTRAINT [FK_StudentDegreePlan_Degree_DegreeId] FOREIGN KEY ([DegreeId]) REFERENCES [Degree] ([DegreeId]) ON DELETE CASCADE,
        CONSTRAINT [FK_StudentDegreePlan_DegreeStatus_DegreeStatusId] FOREIGN KEY ([DegreeStatusId]) REFERENCES [DegreeStatus] ([DegreeStatusId]) ON DELETE CASCADE,
        CONSTRAINT [FK_StudentDegreePlan_Student_StudentId] FOREIGN KEY ([StudentId]) REFERENCES [Student] ([StudentId]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE TABLE [PlanTerm] (
        [PlanTermId] int NOT NULL,
        [StudentDegreePlanId] int NOT NULL,
        [TermAbbrev] nvarchar(20) NULL,
        [TermNumber] int NOT NULL,
        CONSTRAINT [PK_PlanTerm] PRIMARY KEY ([PlanTermId]),
        CONSTRAINT [FK_PlanTerm_StudentDegreePlan_StudentDegreePlanId] FOREIGN KEY ([StudentDegreePlanId]) REFERENCES [StudentDegreePlan] ([StudentDegreePlanId]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE TABLE [PlanTermRequirement] (
        [PlanTermRequirementId] int NOT NULL,
        [PlanTermId] int NOT NULL,
        [RequirementNumber] int NOT NULL,
        [RequirementStatusId] int NOT NULL,
        [TermNumber] int NOT NULL,
        CONSTRAINT [PK_PlanTermRequirement] PRIMARY KEY ([PlanTermRequirementId]),
        CONSTRAINT [FK_PlanTermRequirement_PlanTerm_PlanTermId] FOREIGN KEY ([PlanTermId]) REFERENCES [PlanTerm] ([PlanTermId]) ON DELETE CASCADE,
        CONSTRAINT [FK_PlanTermRequirement_RequirementStatus_RequirementStatusId] FOREIGN KEY ([RequirementStatusId]) REFERENCES [RequirementStatus] ([RequirementStatusId]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE INDEX [IX_AspNetRoleClaims_RoleId] ON [AspNetRoleClaims] ([RoleId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE UNIQUE INDEX [RoleNameIndex] ON [AspNetRoles] ([NormalizedName]) WHERE [NormalizedName] IS NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE INDEX [IX_AspNetUserClaims_UserId] ON [AspNetUserClaims] ([UserId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE INDEX [IX_AspNetUserLogins_UserId] ON [AspNetUserLogins] ([UserId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE INDEX [IX_AspNetUserRoles_RoleId] ON [AspNetUserRoles] ([RoleId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE INDEX [EmailIndex] ON [AspNetUsers] ([NormalizedEmail]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE UNIQUE INDEX [UserNameIndex] ON [AspNetUsers] ([NormalizedUserName]) WHERE [NormalizedUserName] IS NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE INDEX [IX_DegreeRequirement_DegreeId] ON [DegreeRequirement] ([DegreeId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE INDEX [IX_PlanTerm_StudentDegreePlanId] ON [PlanTerm] ([StudentDegreePlanId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE INDEX [IX_PlanTermRequirement_PlanTermId] ON [PlanTermRequirement] ([PlanTermId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE INDEX [IX_PlanTermRequirement_RequirementStatusId] ON [PlanTermRequirement] ([RequirementStatusId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE INDEX [IX_StudentDegreePlan_DegreeId] ON [StudentDegreePlan] ([DegreeId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE INDEX [IX_StudentDegreePlan_DegreeStatusId] ON [StudentDegreePlan] ([DegreeStatusId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    CREATE INDEX [IX_StudentDegreePlan_StudentId] ON [StudentDegreePlan] ([StudentId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20171107020803_DEPLOY')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20171107020803_DEPLOY', N'2.0.0-rtm-26452');
END;

GO

